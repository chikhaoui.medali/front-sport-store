import { InjectionToken } from '@angular/core';

const PROTOCOL = 'http';
const PORT = 8000;

export const OPTIONS: any = {
    apiEndpoint: `${PROTOCOL}://sports-store.api.local:${PORT}/api`
};

export const APP_CONFIG = new InjectionToken<any>(OPTIONS);
