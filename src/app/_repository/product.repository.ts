import { Injectable } from '@angular/core';
import { ProductService } from '../_services/product.service';
import { CategoryService } from '../_services/category.service';
import { Product } from '../_model/product.model';
import { Category } from '../_model/category.model';

@Injectable()
export class ProductRepository {
    private product: Product;
    private products: Product[] = [];
    private categories: Category[] = [];

    constructor(
        private productService: ProductService,
        private categoryService: CategoryService
    ) {
        this.productService.getAllProducts().subscribe(products => {
            this.products = products;
        });
        this.categoryService.getCategories().subscribe(categories => {
            this.categories = categories;
        });
    }

    getProducts(category: Category = null): Product[] {
        return this.products
        .filter(p => category == null || category.id === p.category.id);
    }

    loadProduct(id: number) {
        this.productService.getProduct(id).subscribe(product => {
            this.product = product;
        });
    }

    getProduct(id: number): Product {
        this.loadProduct(id);
        return this.product;
    }

    getCategories(): Category[] {
        return this.categories;
    }

    deleteProduct(id: number) {}
}
