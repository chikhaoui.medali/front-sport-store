import { Injectable } from '@angular/core';
import { Order } from '../_model';
import { OrderService } from '../_services';

@Injectable()
export class OrderRepository {
    private orders: Order[] = [];
    private loaded: boolean = false;

    constructor(private orderService: OrderService) {}

    loadOrders() {
        this.loaded = true;
        this.orderService.getOrders().subscribe(orders => {
            this.orders = orders;
        });
    }

    getOrders(): Order[] {
        if (!this.loaded) {
            this.loadOrders();
        }
        return this.orders;
    }
}
