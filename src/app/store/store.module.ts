import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { StoreComponent } from './store.component';
import { CartSummaryComponent } from './cart-summary/cart-summary.component';
import { CartDetailComponent } from './cart-detail/cart-detail.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { ModelModule } from '../_model/model.module';
import { ProductService, CategoryService, OrderService } from '../_services';
import { APP_CONFIG, OPTIONS } from '../_config';

@NgModule({
  imports: [
    ModelModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule
  ],
  declarations: [
    StoreComponent,
    CartSummaryComponent,
    CartDetailComponent,
    CheckoutComponent
  ],
  providers: [
    ProductService,
    CategoryService,
    OrderService,
    {provide: APP_CONFIG, useValue: OPTIONS}
  ],
  exports: [StoreComponent, CartDetailComponent, CheckoutComponent]
})
export class StoreModule { }
