import { Injectable, Inject } from '@angular/core';
import { Order, OrderAdapter } from '../_model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG } from '../_config';
import { map } from 'rxjs/operators';

@Injectable()
export class OrderService {
    constructor(
        private http: HttpClient,
        private adapter: OrderAdapter,
        @Inject(APP_CONFIG) private config: any) {}

    getOrders(): Observable<Order[]> {
        return this.http.get(`${this.config.apiEndpoint}/admin/cart-lines`).pipe(
            map((order: any) => this.adapter.mapAdapt(order))
        );
    }

    saveOrder(order: Order): Observable<Order> {
        console.log(JSON.stringify(order));
        return this.http.post(`${this.config.apiEndpoint}/save`, order)
        .pipe(
            map((res: any) => res)
        );
    }
}
