import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { ProductAdapter, Product } from '../_model/product.model';
import { APP_CONFIG } from '../_config/app.config';
import { Order } from '../_model';

@Injectable()
export class ProductService {
    constructor(
        private http: HttpClient,
        private adapter: ProductAdapter,
        @Inject(APP_CONFIG) private config: any
        ) {}

    getAllProducts(): Observable<Product[]> {
        return this.http.get(`${this.config.apiEndpoint}/products`)
        .pipe(
            map((data: Product[]) => data.map((product: Product) => this.adapter.adapt(product)))
        )
        ;
    }

    getProduct(id: number): Observable<Product> {
        return this.http.get(`${this.config.apiEndpoint}/products/${id}`)
        .pipe(
            map((product: Product) => this.adapter.adapt(product))
        );
    }
}
