import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CategoryAdapter, Category } from '../_model/category.model';
import { APP_CONFIG } from '../_config/app.config';

const PROTOCOL = 'http';
const PORT = 8000;

@Injectable()
export class CategoryService {
    constructor(
        private http: HttpClient,
        private adapter: CategoryAdapter,
        @Inject(APP_CONFIG) private config: any
        ) {}

    getCategories(): Observable<Category[]> {
        return this.http.get(`${this.config.apiEndpoint}/categories`)
        .pipe(
            map((data: Category[]) => data.map((category: Category) => this.adapter.adapt(category)))
        )
        ;
    }
}
