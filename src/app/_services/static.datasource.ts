import { Injectable } from '@angular/core';
import { Product } from '../_model/product.model';
import { Observable } from 'rxjs';
import { from } from 'rxjs';
import { Order } from '../_model/order.model';
import { Category } from '../_model/category.model';

@Injectable()
export class StaticDataSource {
    private categories: Category[] = [
        new Category(1, 'Category 1'),
        new Category(2, 'Category 2'),
        new Category(3, 'Category 3')
    ];

    private products: Product[] = [
        new Product(1, 'Product 1', this.categories.find(category => category.id === 1), 'Product 1 (Category 1)', 100),
        new Product(2, 'Product 2', this.categories.find(category => category.id === 1), 'Product 2 (Category 1)', 100),
        new Product(3, 'Product 3', this.categories.find(category => category.id === 1), 'Product 3 (Category 1)', 100),
        new Product(4, 'Product 4', this.categories.find(category => category.id === 1), 'Product 4 (Category 1)', 100),
        new Product(5, 'Product 5', this.categories.find(category => category.id === 1), 'Product 5 (Category 1)', 100),
        new Product(6, 'Product 6', this.categories.find(category => category.id === 2), 'Product 6 (Category 2)', 100),
        new Product(7, 'Product 7', this.categories.find(category => category.id === 2), 'Product 7 (Category 2)', 100),
        new Product(8, 'Product 8', this.categories.find(category => category.id === 2), 'Product 8 (Category 2)', 100),
        new Product(9, 'Product 9', this.categories.find(category => category.id === 2), 'Product 9 (Category 2)', 100),
        new Product(10, 'Product 10', this.categories.find(category => category.id === 2), 'Product 10 (Category 2)', 100),
        new Product(11, 'Product 11', this.categories.find(category => category.id === 3), 'Product 11 (Category 3)', 100),
        new Product(12, 'Product 12', this.categories.find(category => category.id === 3), 'Product 12 (Category 3)', 100),
        new Product(13, 'Product 13', this.categories.find(category => category.id === 3), 'Product 13 (Category 3)', 100),
        new Product(14, 'Product 14', this.categories.find(category => category.id === 3), 'Product 14 (Category 3)', 100),
        new Product(15, 'Product 15', this.categories.find(category => category.id === 3), 'Product 15 (Category 3)', 100),
    ];

    getProducts(): Observable<Product[]> {
        return from([this.products]);
    }

    getCategories(): Observable<Category[]> {
        return from([this.categories]);
    }

    saveOrder(order: Order): Observable<Order> {
        console.log(JSON.stringify(order));
        return from([order]);
    }
}
