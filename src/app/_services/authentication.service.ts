import { Injectable, Inject } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG } from '../_config';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthenticationService {
    private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: any) {}

    get isLoggedIn() {
        return this.loggedIn.asObservable();
    }

    login(username: string, password: string) {
        return this.http.post<any>(`${this.config.apiEndpoint}/login_check`, { 'username': username, 'password': password })
        .pipe(
            map(user => {
                if (user && user.token) {
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.loggedIn.next(true);
                }
                return user;
            })
        );
    }

    get authenticated(): boolean {
        return !!(localStorage.getItem('currentUser'));
    }

    logout() {
        // document.body.className = 'hold-transition login-page';
        this.loggedIn.next(false);
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }

    setLoggedIn(isLoggedIn: boolean) {
        this.loggedIn.next(isLoggedIn);
    }
}
