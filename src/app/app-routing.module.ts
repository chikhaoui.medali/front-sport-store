import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { StoreComponent } from './store/store.component';
import { CartDetailComponent } from './store/cart-detail/cart-detail.component';
import { CheckoutComponent } from './store/checkout/checkout.component';
import { StoreFirstGuard } from './_services/store-first.guard';
import { AdminModule } from './admin/admin.module';

const routes: Routes = [
    { path: 'store', component: StoreComponent, canActivate: [StoreFirstGuard] },
    { path: 'cart', component: CartDetailComponent, canActivate: [StoreFirstGuard] },
    { path: 'checkout', component: CheckoutComponent, canActivate: [StoreFirstGuard] },
    {
        path: 'admin',
        loadChildren: () => AdminModule
    },
    { path: '**', pathMatch: 'full', redirectTo: '/store' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
