import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../_services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(
    private authentificationService: AuthenticationService,
    private router: Router) { }

  ngOnInit() {
  }

  logout() {
    this.authentificationService.logout();
    this.router.navigateByUrl('/');
  }

}
