import { Component, OnInit } from '@angular/core';
import { Order } from '../../_model';
import { OrderRepository } from '../../_repository';

@Component({
  selector: 'app-order-table',
  templateUrl: './order-table.component.html',
  styleUrls: ['./order-table.component.css']
})
export class OrderTableComponent implements OnInit {
  includeShipped = false;
  public orders: Array<Order>;
  public count: number;

  constructor(private repository: OrderRepository) {}

  ngOnInit() {}

  getOrders(): Order[] {
    return this.repository.getOrders().filter(o => this.includeShipped || !o.shipped);
  }

  markShipped(order: Order) {
    order.shipped = true;
  }

  delete(id: number) {
    console.log(id);
  }

}
