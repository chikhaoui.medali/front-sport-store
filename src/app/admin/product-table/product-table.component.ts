import { Component, OnInit } from '@angular/core';
import { ProductRepository } from 'src/app/_repository';
import { Product } from 'src/app/_model';

@Component({
  selector: 'app-product-table',
  templateUrl: './product-table.component.html',
  styleUrls: ['./product-table.component.css']
})
export class ProductTableComponent implements OnInit {

  constructor(private repository: ProductRepository) {}

  ngOnInit() {
  }

  getProducts(): Product[] {
    return this.repository.getProducts();
  }

  deleteProduct(id: number) {
    this.repository.deleteProduct(id);
  }

}
