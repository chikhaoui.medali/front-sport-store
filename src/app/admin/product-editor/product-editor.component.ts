import { Component, OnInit } from '@angular/core';
import { Product, Category } from '../../_model';
import { ActivatedRoute } from '@angular/router';
import { ProductService, CategoryService } from '../../_services';
import { FormGroup, FormControl, RequiredValidator, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-product-editor',
  templateUrl: './product-editor.component.html',
  styleUrls: ['./product-editor.component.css']
})
export class ProductEditorComponent implements OnInit {

  editing: boolean = false;
  product: Product = new Product();
  categories: Category[];
  categorySelectedValue: Category = null;
  productForm: FormGroup;

  constructor(
    private productService: ProductService,
    private categoryService: CategoryService,
    private activeRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.editing = this.activeRoute.snapshot.params['mode'] === 'edit';
    this.productForm = this.createFormGroup(this.fb);
    this.categoryService.getCategories().subscribe(categories => {
      this.categories = categories;
    });
    if (this.editing) {
      this.productService.getProduct(this.activeRoute.snapshot.params['id']).subscribe(data => {
        this.product = data;
        this.productForm.setValue({
          product: this.product
        });
        this.categorySelectedValue = data.category;
      });
    }
  }

  createFormGroup(fb: FormBuilder) {
    return fb.group({
      product: fb.group(
        this.product
      )
    });
  }

  compare(val1: any, val2: any) {
    return (val1 !== null) && (val2 !== null) && (val1.id === val2.id);
  }

  save() {
    console.log('submit');
    console.log(this.productForm.value);
  }

}
