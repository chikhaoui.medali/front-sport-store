import { Injectable } from '@angular/core';
import { Cart } from './cart.model';
import { Product } from './product.model';
import { Customer } from './customer.model';
import { MapAdapter } from './map.adapter';

@Injectable()
export class Order {
    constructor(
        public id?: number,
        public createdAt?: Date,
        public shipped?: boolean,
        public customer?: Customer,
        public products?: any[],
        public cart?: Cart
    ) {}

    clear() {
        this.id = null;
        this.createdAt = null;
        this.shipped = false;
        this.customer.clear();
        this.products = [];
        this.cart.clear();
    }
}

@Injectable({
    providedIn: 'root'
})
export class OrderAdapter implements MapAdapter<Order> {
    private orders = [];

    mapAdapt(items: Array<any>): Array<Order> {
        this.orders = [];
        items.forEach((item, index) => {
            this.orders.push(new Order(
                item.id,
                item.created_date,
                item.shipped,
                item.customer,
                item.products
            ));
        });

        return this.orders;
    }
}

