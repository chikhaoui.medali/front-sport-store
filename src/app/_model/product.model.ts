import { Injectable } from '@angular/core';
import { Adapter } from './adapter';
import { Category } from './category.model';

export class Product {
    constructor(
        public id?: number,
        public name?: string,
        public category?: Category,
        public description?: string,
        public price?: number
    ) { }

    public deserialiser(item: any) {
        Object.assign(this, item);
    }
}

@Injectable({
    providedIn: 'root'
})
export class ProductAdapter implements Adapter<Product> {
    adapt(item: any): Product {
        return new Product(
            item.id,
            item.name,
            item.category,
            item.description,
            item.price
        );
    }
}
