import { NgModule } from '@angular/core';
import { Cart, Order } from '.';
import { ProductRepository, OrderRepository } from '../_repository';
import { StaticDataSource } from '../_services';

@NgModule({
  providers: [ProductRepository, Cart, Order, StaticDataSource, OrderRepository]
})
export class ModelModule { }
