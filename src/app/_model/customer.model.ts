export class Customer {
    constructor(
        public id?: number,
        public name?: string,
        public address?: string,
        public city?: string,
        public state?: string,
        public country?: string,
        public zip?: string
    ) {}

    clear() {
        this.name = this.address = this.city = null;
        this.state = this.zip = this.country = null;
    }
}
