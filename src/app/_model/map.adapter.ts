export interface MapAdapter<T> {
    mapAdapt(item: Array<any>): Array<T>;
}
