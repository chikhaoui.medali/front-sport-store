import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthenticationService } from '../_services';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

/**
 * Il est implémenté à l'aide de la classe HttpInterceptor introduite dans Angular 4.3 dans le nouveau module HttpClientModule.
 * En étendant la classe HttpInterceptor, vous pouvez créer un intercepteur personnalisé
 * pour intercepter toutes les réponses d'erreur du serveur à un emplacement unique.
 */
@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    /**
     * L'intercepteur d'erreur intercepte les réponses http de l'API pour vérifier s'il y a eu des erreurs.
     * S'il y a une réponse 401 non autorisée, l'utilisateur est automatiquement déconnecté de l'application,
     * toutes les autres erreurs sont réémises pour être capturées par le service appelanta
     * fin qu'une alerte puisse être affichée à l'utilisateur.
     */
    constructor(private authService: AuthenticationService, private router: Router) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401) {
                // auto logout if 401 response returned from api
                this.authService.logout();
                location.reload(true);
            }
            const error = err.error.message || err.statusText;
            return throwError(error);
        }));
    }
}
